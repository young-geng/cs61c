#include <stdio.h>

void swap(unsigned char *x, unsigned char *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void flip_horizontal(unsigned char *arr, int dim) {
  int x, y;
  for (y = 0; y < dim; y += 1) {
    for (x = 0; x < dim / 2; x += 1) {
      swap(&arr[dim * y + x], &arr[dim * y + dim - 1 - x]);
    }
  }
}

void print_2D(unsigned char *arr, int dim) {
  char *format = "%02x ";
  char *format_nl = "%02x\n";
  int i = 0;
  for (i = 0; i < dim*dim; i++) {
    printf( ((i + 1) % dim) ? format : format_nl, arr[i]);
  }
  printf("\n");
}

int main() {
  int dim = 5, i;
  unsigned char arr[] = {0x00, 0xff, 0xff, 0xff, 0x00,
                         0x00, 0x00, 0x00, 0xff, 0x00,
                         0x00, 0xff, 0xff, 0xff, 0x00,
                         0x00, 0x00, 0x00, 0xff, 0x00,
                         0x00, 0xff, 0xff, 0xff, 0x00};

  printf("Before flip:\n");
  print_2D(arr, dim);

  flip_horizontal(arr, dim);

  printf("After flip:\n");
  print_2D(arr, dim);

  return 0;
}
