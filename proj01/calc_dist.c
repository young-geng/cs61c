/*
 * PROJ1-1: YOUR TASK A CODE HERE
 *
 * You MUST implement the calc_min_dist() function in this file.
 *
 * You do not need to implement/use the swap(), flip_horizontal(), transpose(), or rotate_ccw_90()
 * functions, but you may find them useful. Feel free to define additional helper functions.
 */

#include <stdlib.h>
#include <stdio.h>
#include "digit_rec.h"
#include "utils.h"
#include "limits.h"


int map(int x, int y, int width) {
    return y * width + x;
}

unsigned int min(unsigned int first, unsigned int second) {
    return (first < second) ? first : second;
}



/* Swaps the values pointed to by the pointers X and Y. */
void swap(unsigned char *x, unsigned char *y) {
    /* Optional function */
    unsigned char temp = *x;
    *x = *y;
    *y = temp;
}

/* Flips the elements of a square array ARR across the y-axis. */

void flip_horizontal(unsigned char *arr, int width) {
  int x, y;
  for (y = 0; y < width; y += 1) {
    for (x = 0; x < width / 2; x += 1) {
      swap(&arr[width * y + x], &arr[width * y + width - 1 - x]);
    }
  }
}






/* Transposes the square array ARR. */
void transpose(unsigned char *arr, int width) {
    int i, j;
    for (i = 1; i < width; i++) {
        for (j = 0; j < i; j++) {
            swap(&arr[map(i, j, width)], &arr[map(j, i, width)]);
        }
    }
}

void flip_vertical(unsigned char *arr, int width) {
    transpose(arr, width);
    flip_horizontal(arr, width);
    transpose(arr, width);
}

/* Rotates the square array ARR by 90 degrees counterclockwise. */
void rotate_90(unsigned char *arr, int width) {
    /* Optional function */
    flip_horizontal(arr, width);
    transpose(arr, width);
}

void rotate_180(unsigned char *arr, int width) {
    rotate_90(arr, width);
    rotate_90(arr, width);
}

void rotate_270(unsigned char *arr, int width) {
    rotate_180(arr, width);
    rotate_90(arr, width);
}




unsigned char* extract(unsigned char* arr, int width, int x, int y, int size) {
    int i, j;
    unsigned char* new_arr = (unsigned char*)malloc(size * size * sizeof(unsigned char));
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            new_arr[map(j, i, size)] = arr[map(x + j, y + i, width)];
        }
    }
    return new_arr;
}


unsigned int extract_compare(unsigned char *image, unsigned char *template, int width) {
    int dist = 0, a, b;
    for (int i = 0; i < width * width; i++) {
        a = (int)image[i];
        b = (int)template[i];
        dist += ((a - b) * (a - b));
    }
    return (unsigned int)dist;
}

/* generate all the possible combination of template */
unsigned char** generate_templates(unsigned char* template, int width) {
    unsigned char** result = (unsigned char**)malloc(8 * sizeof(unsigned char*));
    for (int i = 0; i < 8; i++) {
        result[i] = (unsigned char*)malloc(width * width * sizeof(unsigned char));
    }
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < width * width; j++) {
            result[i][j] = template[j];
        }
    }
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < i % 4; j++) {
            rotate_90(result[i], width);
        }
    }
    for (int i = 4; i < 8; i++) {
        flip_horizontal(result[i], width);
    }
    return result;

}


/* Returns the squared Euclidean distance between TEMPLATE and IMAGE. The size of IMAGE
 * is I_WIDTH * I_HEIGHT, while TEMPLATE is square with side length T_WIDTH. The template
 * image should be flipped, rotated, and translated across IMAGE.
 */
unsigned int calc_min_dist(unsigned char *image, int i_width, int i_height, 
        unsigned char *template, int t_width) {
    unsigned char* temp;
    unsigned int minimum = UINT_MAX;
    for (int y = 0; y <= i_height - t_width; y++) {
        for (int x = 0; x <= i_width - t_width; x++) {
            temp = extract(image, i_width, x, y, t_width);
            for (int i = 0; i < 4; i++) {
                minimum = min(extract_compare(temp, template, t_width), minimum);
                rotate_90(temp, t_width);
            }
            flip_horizontal(temp, t_width);
            for (int i = 0; i < 4; i++) {
                minimum = min(extract_compare(temp, template, t_width), minimum);
                rotate_90(temp, t_width);
            }
            free(temp);
        }
    }
    return minimum;
}   


