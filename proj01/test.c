#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "calc_dist_new.c"

print_matrix(unsigned char* arr, int width) {
    int i, j;
    for (i = 0; i < width; i++) {
        for (j = 0; j < width; j++) {
            printf("%d ", (unsigned int)arr[map(j, i, width)]);
        }
        printf("\n");
    }
}


int main() {
    unsigned char arr[16];
    unsigned char* new_arr;
    int i;
    for (i = 0; i < 16; i++) {
        arr[i] = (unsigned char)i;
    }
    //flip_vertical(arr, 4);
    new_arr = extract(arr, 4, 1, 1, 3);
    print_matrix(new_arr, 3);
    return 0;

}