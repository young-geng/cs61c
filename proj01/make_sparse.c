/*
 * PROJ1-1: YOUR TASK B CODE HERE
 * 
 * Feel free to define additional helper functions.
 */

#include <stdlib.h>
#include <stdio.h>
#include "sparsify.h"
#include "utils.h"

/* Returns a NULL-terminated list of Row structs, each containing a NULL-terminated list of Elem structs.
 * See sparsify.h for descriptions of the Row/Elem structs.
 * Each Elem corresponds to an entry in dense_matrix whose value is not 255 (white).
 * This function can return NULL if the dense_matrix is entirely white.
 */

Elem* new_elem(int x, unsigned char value, Elem* next) {
    Elem* e = (Elem*)malloc(sizeof(Elem));
    e->x = x;
    e->value = value;
    e->next = next;
}

Row* new_row(int y, Elem* elems, Row* next) {
    Row* e = (Row*)malloc(sizeof(Row));
    e->y = y;
    e->elems = elems;
    e->next = next;
}

Elem* make_element_list(unsigned char* arr, int width) {
    int i = 0;
    Elem *res = new_elem(-1, (unsigned char)0, NULL);
    Elem* ptr = res;
    for (i = 0; i < width; i++) {
        if ((int)arr[i] != 255) {
            ptr->next = new_elem(i, arr[i], NULL);
            ptr = ptr->next;
        }
    }
    ptr = res->next;
    free(res);
    return ptr;
}


Row *dense_to_sparse(unsigned char *dense_matrix, int width, int height) {
    /* YOUR CODE HERE */
    int i;
    Elem* elems;
    Row* res = new_row(-1, NULL, NULL);
    Row* ptr = res;
    for (i = 0; i < height; i++) {
        elems = make_element_list(&dense_matrix[i * width], width);
        if (elems != NULL) {
            ptr->next = new_row(i, elems, NULL);
            ptr = ptr->next;
        }
    }
    ptr = res->next;
    free(res);
    return ptr;
}

void free_elems(Elem* e) {
    Elem* tmp = e;
    while (e != NULL) {
        e = e->next;
        free(tmp);
        tmp = e;
    }
}

/* Frees all memory associated with SPARSE. SPARSE may be NULL. */
void free_sparse(Row *sparse) {
    /* YOUR CODE HERE */
    Row* rptr = sparse;
    while (rptr != NULL) {
        rptr = rptr->next;
        free_elems(sparse->elems);
	   free(sparse);
        sparse = rptr;
    }
}

