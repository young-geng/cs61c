#include "matcher.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

/**
 * Implementation of your matcher function, which
 * will be called by the main program.
 *
 * You may assume that both line and pattern point
 * to reasonably short, null-terminated strings.
 */

#define false 0
#define true 1
#define bool int

typedef struct {
    int is_dot;
    int is_repeat;
    char cont;
    int least;
    int most;
    int is_end;
} rechar;

void init_rechar(rechar* r, char cont) {
    r->is_dot = 0;
    r->is_repeat = 0;
    r->least = 0;
    r->most = 0;
    r->is_end = cont == '\0';
    r->cont = cont;
}

rechar* to_rechar(char *pattern) {
    int pos = 0, i = 0;
    rechar *rc = (rechar*)malloc(5000 * sizeof(rechar));
    rechar *cur = rc;
    char tmp[100];
    while (pos < strlen(pattern)) {
        if (pattern[pos] != '\\' && pattern[pos] != '.' && pattern[pos] != '{') {
            init_rechar(cur, pattern[pos]);
            pos++;
            cur++;
        } else if (pattern[pos] == '\\') {
            pos++;
            init_rechar(cur, pattern[pos]);
            pos++;
            cur++;
        } else if (pattern[pos] == '.') {
            init_rechar(cur, pattern[pos]);
            cur->is_dot = true;
            pos++;
            cur++;
        } else if (pattern[pos] == '{') {
            pos++;
            cur--;
            cur->is_repeat = true;
            i = 0;
            while (pattern[pos] <= '9' && pattern[pos] >= '0') {
                tmp[i] = pattern[pos];
                pos++;
                i++;
            }
            tmp[i] = '\0';
            cur->least = atoi(tmp);
            pos++;
            if (pattern[pos] == '}') {
                cur->most = 5000;
                cur++;
                pos++;
                continue;
            }
            i = 0;
            while (pattern[pos] <= '9' && pattern[pos] >= '0') {
                tmp[i] = pattern[pos];
                pos++;
                i++;
            }
            tmp[i] = '\0';
            cur->most = atoi(tmp) + cur->least;
            cur++;
            pos++;
        }
    }
    init_rechar(cur, '\0');
    return rc;
}

int match(char *line, rechar* pattern) {
    int lp = 0;
    int i;
    if (pattern[0].is_end) {
        return true;
    } else if (line[0] == '\0') {
        return false;
    }
    if (!pattern[0].is_dot && !pattern[0].is_repeat) {
        if (line[0] != pattern[0].cont) {
            return false;
        }
        return match(&line[1], &pattern[1]);
    } else if (pattern[0].is_dot && !pattern[0].is_repeat) {
        return match(&line[1], &pattern[1]);
    } else if (!pattern[0].is_dot && pattern[0].is_repeat) {
        for (i = 1; i <= pattern[0].least; i++) {
            if (line[lp] == '\0' || line[lp] != pattern[0].cont) {
                return false;
            }
            lp++;
        }
        if (match(&line[lp], &pattern[1])) {
            return true;
        }
        for (i = 0; i < pattern[0].most - pattern[0].least; i++) {
            if (line[lp] == '\0' || line[lp] != pattern[0].cont) {
                return false;
            }
            lp++;
            if (match(&line[lp], &pattern[1])) {
                return true;
            }
        }
        return false;
    } else if (pattern[0].is_dot && pattern[0].is_repeat) {
        for (i = 1; i <= pattern[0].least; i++) {
            if (line[lp] == '\0') {
                return false;
            }
            lp++;
        }
        if (match(&line[lp], &pattern[1])) {
            return true;
        }
        for (i = 0; i < pattern[0].most - pattern[0].least; i++) {
            if (line[lp] == '\0') {
                return false;
            }
            lp++;
            if (match(&line[lp], &pattern[1])) {
                return true;
            }
        }
        return false;
    }
    return false;
}

int rgrep_matches(char *line, char *pattern) {

    //
    // TODO put your code here.
    //
    rechar *re = to_rechar(pattern);
    int i = 0;
    for (i = 0; i < strlen(line); i++) {
        if (match(&line[i], re)) {
            free(re);
            return true;
        }
    }
    free(re);
    return false;
}
    

