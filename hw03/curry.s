.data
code: .word 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xadcd0080, 0x00000000, 0x01800008, 0x00000000, 0x00000000, 0xadc40084, 0x25c20030, 0x03e00008, 0x00000000, 0x00000000, 0xadc40088, 0x25c20044, 0x03e00008, 0x00000000, 0x00000000, 0xadc4008c, 0x25c20058, 0x03e00008, 0x8dc5008c, 0x8dc60088, 0x8dc70084, 0x8dc80080, 0x27bdfffc, 0xafbf0000, 0x0100f809, 0x8fbf0000, 0x27bd0004, 0x03e00008
.text
# Your code here. Remember that you need to turn on self-modifying mode in order for
# Mars to allow self-modifying mips.
# $a0 corresponds to destination buffer for the curryable-ized function
# $a1 corresponds to the function to be curryable-ized
# $a2 corresponds to the number of arguments to the function to be curryable-ized
curry:
	li $t1, 32
	move $t0, $0
	move $t2, $a0
	la $t3, code
 loop:	beq $t0, $t1, done_l
	lw $t4, 0($t3)
	sw $t4, 0($t2)
	addiu $t3, $t3, 4
	addiu $t2, $t2, 4
	addiu $t0, $t0, 1
	j loop
 done_l:	
	# Store the inst <lui $t6, upper_dest> into $t4 
	move $t0, $a0
	srl $t0, $t0, 16
	lui $t4, 0x3c0e
	or $t4, $t4, $t0

	# Store the inst <ori $t6, $t6, lower_dest> into $t5
	move $t0, $a0
	sll $t0, $t0, 16
	srl $t0, $t0, 16
	lui $t5, 0x35ce
	or $t5, $t5, $t0

	# Store the inst <lui $t5, upper_func> into $t6
	move $t0, $a1
	srl $t0, $t0, 16
	lui $t6, 0x3c0d
	or $t6, $t6, $t0

	# Store the inst <ori $t5, $t5, lower_func> into $t7
	move $t0, $a1
	sll $t0, $t0, 16
	srl $t0, $t0, 16
	lui $t7, 0x35ad
	or $t7, $t7, $t0

	# Fill the 0, 28, 48, 68 place with inst <lui $t6, upper_dest>
	sw $t4, 0($a0)
	sw $t4, 28($a0)
	sw $t4, 48($a0)
	sw $t4, 68($a0)

	# Fill the 4, 32, 52, 72 place with inst <ori $t6, $t6, lower_dest>
	sw $t5, 4($a0)
	sw $t5, 32($a0)
	sw $t5, 52($a0)
	sw $t5, 72($a0)

	# Fill the 8 place with inst <lui $t5, upper_func>
	sw $t6, 8($a0)

	# Fill the 12 place with inst <ori $t5, $t5, lower_func>
	sw $t7 12($a0)

	# Fix the jump address
	beq $a2, 1, one
	beq $a2, 2, two
	beq $a2, 3, three
	beq $a2, 4, four

 one:
	li $t1, 88
	j done_j
 two:
	li $t1, 68
	j done_j

 three:
	li $t1, 48
	j done_j

 four:
	li $t1, 28
	j done_j
 
 done_j:
	lui $t0, 0x25cc
	or $t1, $t1, $t0
	sw $t1, 20($a0)		

	jr $ra

#This is the destination buffer we'll be using for your curryable-ized function.
#I found it helpful to scratch out what lines of code I'd be having my code
#write to this buffer before writing curry. Feel free to add more nops to this
#buffer if you need to, but know that your code should only need O(1) space for
#any function we hand you. If you find yourself wanting more than constant space
#you're probably doing something wrong.
destination:
	nop #
	nop #
        nop #
        nop #
        nop #

	nop #
	nop #
        nop #
        nop #
        nop #

	nop #
	nop #
        nop #
        nop #
        nop #

	nop #
	nop #
        nop #
        nop #
        nop #

        nop #
        nop #
        nop #
        nop #
        nop #
        nop #
        nop #
        nop #

        nop #
        nop #
        nop #
        nop #
        nop #
