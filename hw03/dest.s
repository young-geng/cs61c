dest:	nop # lui $t6, upper_dest # The upper address of destination, determined at run time, FIXME
	nop # ori $t6, $t6, lower_dest # The lower address of destination, FIXME
	nop # lui $t5, upper_func	# Save the address of the function
	nop # ori $t5, lower_func
	sw $t5, 128($t6) 
	nop # addiu $t4, $t6, position_on_arg_nums # Increment to the code address, determined at runtime, by args num
	jr $t4
four:	nop # lui $t6, upper_dest # The upper address of destination, determined at run time, FIXME
	nop # ori $t6, $t6, lower_dest # The lower address of destination, FIXME
	sw $a0, 132($t6) # Arg 3
	addiu $v0, $t6, 48 # Return the address of three_left
	jr $ra
three:	nop # lui $t6, upper_dest # The upper address of destination, determined at run time, FIXME
	nop # ori $t6, $t6, lower_dest # The lower address of destination, FIXME
	sw $a0, 136($t6) # Arg 2
	addiu $v0, $t6, 68 # Return the address of two_left
	jr $ra
two:	nop # lui $t6, upper_dest # The upper address of destination, determined at run time, FIXME
	nop # ori $t6, $t6, lower_dest # The lower address of destination, FIXME
	sw $a0, 140($t6) # Arg 1
	addiu $v0, $t6, 88 # Return the address of compute
	jr $ra
cpt:	lw $a1, 140($t6)
	lw $a2, 136($t6)
	lw $a3, 132($t6)
	lw $t0, 128($t6) # The address if the function
	addiu $sp, $sp -4
	sw $ra, 0($sp)
	jalr $t0
	lw $ra 0($sp)
	addiu $sp, $sp, 4
	jr $ra


# 0-127: Code
# 128:   Function
# 132:	 arg 3
# 136:   arg 2
# 140:	 arg 1