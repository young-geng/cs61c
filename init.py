#! /usr/bin/env python
# cs61c lab / homework / project initialization script for cs61c-hn
# This program should be executed with python 2.x
import os
import sys
import re

if len(sys.argv) != 2:
    print "input error!"
    exit(1)


if sys.argv[1] == "commit":
    os.system("git add .")
    os.system('git commit -m "automatic commit"')
    os.system('git push')
    exit(0)

if sys.argv[1] == "pull":
    os.system("git pull origin master")
    exit(0)

if sys.argv[1] == "stash":
    os.system("git stash") 
    exit(0)


match = re.findall("(lab|hw|proj)([0-1][0-9])", sys.argv[1])
if len(match) != 1:
    print "Input error!"
    exit(1)

match = match[0]

if match[0] == "lab":
    cmd0 = "mkdir " + match[0] + match[1]
    cmd1 = "scp -r cs61c-nh@cory.cs.berkeley.edu:~cs61c/labs/" + match[1] +"/* " + match[0] + match[1] + "/"
elif match[0] == "hw":
    cmd0 = "mkdir " + match[0] + match[1]
    cmd1 = "scp -r cs61c-nh@cory.cs.berkeley.edu:~cs61c/hw/" + match[1] +"/* " + match[0] + match[1] + "/"
elif match[0] == "proj":
    cmd0 = "mkdir " + match[0] + match[1]
    cmd1 = "scp -r cs61c-nh@cory.cs.berkeley.edu:~cs61c/proj/" + match[1] +"/* " + match[0] + match[1] + "/"
else:
    print "Input error!"
    exit(1)

os.system(cmd0)
os.system(cmd1)


