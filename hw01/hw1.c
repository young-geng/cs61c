#include <stdio.h>
#include <stdlib.h>
#define YES ("a palindrome")
#define NO ("NOT a palindrome")
#define CHECK(X) (is_binary_palindrome(X) ? YES : NO)
#define int_length 32

int is_binary_palindrome(unsigned int n);

int main(int argc, char *argv[]) {
  if (argc == 1) {
  const char testStr[] = "Program thinks %d is %s, should be %s\n";
  printf (testStr, 0, CHECK(0) , YES);
  printf (testStr, 11, CHECK(11), NO);
  printf (testStr, 2863289685u, CHECK(2863289685u), YES);
  printf (testStr, 268435456, CHECK(268435456), NO);
  printf (testStr, 9, CHECK(9), YES);
  } else if (argc == 2) {
    unsigned int number = (unsigned int)strtoul(argv[1], NULL, 10);
    printf("%d\n", is_binary_palindrome(number));

  } else {
    printf("too many arguments!\n");
  }
  return 0;
}

/** Returns 1 iff the binary numeral representing N is a palindrome.
 *  Ignores leading 0s. */
int is_binary_palindrome(unsigned int n) {
    int num[int_length];
    my_uint_to_array(n, num);
    int length = find_max_pos(num);
    if (length <= 0)
        return 1;
    else {
        int i = 0, j = length;
        unsigned int flag = 1;
        while (i < j) {
            if (num[i] != num[j])
                flag = 0;
            i++;
            j--;
        }
        return flag;
    }
}

void my_uint_to_array(unsigned int x, unsigned int a[]) {
    unsigned int cst1 = 1, i;
    for (i = 0; i < int_length; i++) {
        a[i] = (x & (cst1 << i)) >> i;
    }
}

int find_max_pos(unsigned int x[]) {
    int i;
    for (i = int_length - 1; i >= 0; i--) {
        if (x[i]) {
            return i;
        }
    }
    return i;
} 
