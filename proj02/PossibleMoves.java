/*
 * CS61C Spring 2014 Project2
 * Reminders:
 *
 * DO NOT SHARE CODE IN ANY WAY SHAPE OR FORM, NEITHER IN PUBLIC REPOS OR FOR DEBUGGING.
 *
 * This is one of the two files that you should be modifying and submitting for this project.
 */
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang.Math;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class PossibleMoves {
    public static class Map extends Mapper<IntWritable, MovesWritable, IntWritable, IntWritable> {
        int boardWidth;
        int boardHeight;
        boolean OTurn;
        /**
         * Configuration and setup that occurs before map gets called for the first time.
         *
         **/
        @Override
        public void setup(Context context) {
            // load up the config vars specified in Proj2.java#main()
            boardWidth = context.getConfiguration().getInt("boardWidth", 0);
            boardHeight = context.getConfiguration().getInt("boardHeight", 0);
            OTurn = context.getConfiguration().getBoolean("OTurn", true);
        }



        /**
         * The map function for the first mapreduce that you should be filling out.
         */
        @Override
        public void map(IntWritable key, MovesWritable val, Context context) throws IOException, InterruptedException {
            /* YOU CODE HERE */
            //System.out.println("==========PossibleMoves===MAP==========");
            //System.out.println("Key:     |" + Proj2Util.gameUnhasher(key.get(), boardWidth, boardHeight));
            if (val.getStatus() != 0) {
                return;
            }
            String board = Proj2Util.gameUnhasher(key.get(), boardWidth, boardHeight);
            for (int i = 0; i < boardWidth; i++) {
                char[][] tempBoard = boardToArray(board);
                if (tempBoard[i][boardHeight - 1] != ' ') {
                    continue;
                }
                int j;
                for (j = boardHeight - 1; j >= 0 && tempBoard[i][j] == ' '; j--);
                j++;
                if(OTurn) {
                    tempBoard[i][j] = 'O';
                } else {
                    tempBoard[i][j] = 'X';
                }
                context.write(boardToIntWritable(tempBoard), key);
                //System.out.println("Out:     |" + Proj2Util.gameUnhasher(boardToIntWritable(tempBoard).get(), boardWidth, boardHeight) + "  , |" + Proj2Util.gameUnhasher(key.get(), boardWidth, boardHeight));

            }



        }

        private char[][] boardToArray(String state) {
            char[] raw = state.toCharArray();
            char[][] array = new char[boardWidth][boardHeight];
            int total = 0;
            for (int i = 0; i < boardWidth; i++) {
                for (int j = 0; j < boardHeight; j++) {
                    array[i][j] = raw[i * boardHeight + j];
                }
            }
            return array;
        }

        private IntWritable boardToIntWritable(char[][] board) {
            char[] raw = new char[boardWidth * boardHeight];
            for (int i = 0; i < boardWidth; i++) {
                for (int j = 0; j < boardHeight; j++) {
                    raw[i * boardHeight + j] = board[i][j];
                }
            }
            return new IntWritable(Proj2Util.gameHasher(new String(raw), boardWidth, boardHeight));
        }


    }

    public static class Reduce extends Reducer<IntWritable, IntWritable, IntWritable, MovesWritable> {

        int boardWidth;
        int boardHeight;
        int connectWin;
        boolean OTurn;
        boolean lastRound;
        /**
         * Configuration and setup that occurs before reduce gets called for the first time.
         *
         **/
        @Override
        public void setup(Context context) {
            // load up the config vars specified in Proj2.java#main()
            boardWidth = context.getConfiguration().getInt("boardWidth", 0);
            boardHeight = context.getConfiguration().getInt("boardHeight", 0);
            connectWin = context.getConfiguration().getInt("connectWin", 0);
            OTurn = context.getConfiguration().getBoolean("OTurn", true);
            lastRound = context.getConfiguration().getBoolean("lastRound", true);
        }

        private boolean isDraw(IntWritable state) {
            String board = Proj2Util.gameUnhasher(state.get(), boardWidth, boardHeight);
            return board.indexOf(" ") == -1;
        }

        private String unHash(int hash) {
            return Proj2Util.gameUnhasher(hash, boardWidth, boardHeight);
        }

        private String unHash(IntWritable hash) {
            return Proj2Util.gameUnhasher(hash.get(), boardWidth, boardHeight);
        }


        /**
         * The reduce function for the first mapreduce that you should be filling out.
         */
        @Override
        public void reduce(IntWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            //System.out.println("==========PossibleMoves===REDUCE==========");
            //System.out.println("Key:     |" + unHash(key));
            ArrayList<Integer> list = new ArrayList<Integer>();
            for (IntWritable i : values) {
                list.add(i.get());
                //System.out.println("Value:   |" + unHash(i.get()));
            }
            int[] moves = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                moves[i] = list.get(i);
            }
            //int steps = count(key);
    
            if (Proj2Util.gameFinished(Proj2Util.gameUnhasher(key.get(), boardWidth, boardHeight), boardWidth, boardHeight, connectWin)) {
                if (OTurn) {
                    context.write(key, new MovesWritable(1, 0, moves));
                    //System.out.println("Out:     |" + unHash(key) + "   " + 1);
                } else {
                    context.write(key, new MovesWritable(2, 0, moves));
                    //System.out.println("Out:     |" + unHash(key) + "   " + 2);
                }
            } else if (isDraw(key)) {
                context.write(key, new MovesWritable(3, 0, moves));
                //System.out.println("Out:     |" + unHash(key) + "   " + 3);
            } else {
                context.write(key, new MovesWritable(0, 0, moves));
                //System.out.println("Out:     |" + unHash(key) + "   " + 0);
            }


        }
    }
}
