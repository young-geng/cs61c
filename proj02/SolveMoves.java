/*
 * CS61C Spring 2014 Project2
 * Reminders:
 *
 * DO NOT SHARE CODE IN ANY WAY SHAPE OR FORM, NEITHER IN PUBLIC REPOS OR FOR DEBUGGING.
 *
 * This is one of the two files that you should be modifying and submitting for this project.
 */
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang.Math;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class SolveMoves {
    public static class Map extends Mapper<IntWritable, MovesWritable, IntWritable, ByteWritable> {
        /**
         * Configuration and setup that occurs before map gets called for the first time.
         *
         **/
        @Override
        public void setup(Context context) {
        }

        /**
         * The map function for the second mapreduce that you should be filling out.
         */
        @Override
        public void map(IntWritable key, MovesWritable val, Context context) throws IOException, InterruptedException {
            /* YOUR CODE HERE */
            //System.out.println("==========SolveMoves MAP=============");
            //System.out.println("Key:     |" + Proj2Util.gameUnhasher(key.get(), 2, 2));

            int[] moves = val.getMoves();
            byte value = val.getValue();
            //System.out.println("Value:   "  + value);
            //System.out.println("Value: --  "  + (new ByteWritable(value)).get());
            for (int i = 0; i < moves.length; i++) {
                //System.out.println("Out:     |" + Proj2Util.gameUnhasher(moves[i], 2, 2));
                context.write(new IntWritable(moves[i]), new ByteWritable(value));
            }



        }
    }

    public static class Reduce extends Reducer<IntWritable, ByteWritable, IntWritable, MovesWritable> {

        int boardWidth;
        int boardHeight;
        int connectWin;
        boolean OTurn;
        /**
         * Configuration and setup that occurs before map gets called for the first time.
         *
         **/
        @Override
        public void setup(Context context) {
            // load up the config vars specified in Proj2.java#main()
            boardWidth = context.getConfiguration().getInt("boardWidth", 0);
            boardHeight = context.getConfiguration().getInt("boardHeight", 0);
            connectWin = context.getConfiguration().getInt("connectWin", 0);
            OTurn = context.getConfiguration().getBoolean("OTurn", true);
        }


        private char[][] boardToArray(IntWritable hash) {
            String state = Proj2Util.gameUnhasher(hash.get(), boardWidth, boardHeight);
            char[] raw = state.toCharArray();
            char[][] array = new char[boardWidth][boardHeight];
            int total = 0;
            for (int i = 0; i < boardWidth; i++) {
                for (int j = 0; j < boardHeight; j++) {
                    array[i][j] = raw[i * boardHeight + j];
                }
            }
            return array;
        }

        private int boardToInt(char[][] board) {
            return Proj2Util.gameHasher(boardToString(board), boardWidth, boardHeight);
        }

        private String boardToString(char[][] board) {
            char[] raw = new char[boardWidth * boardHeight];
            for (int i = 0; i < boardWidth; i++) {
                for (int j = 0; j < boardHeight; j++) {
                    raw[i * boardHeight + j] = board[i][j];
                }
            }
            return new String(raw);
        }

        private String b2s(char[][] board) {
            String x = "";
            for (int i = 0; i < boardWidth; i++) {
                for (int j = 0; j < boardHeight; j++) {
                    x += board[i][j];
                }
            }
            return x;
        }

        private boolean isValidParent(Iterable<Byte> values) {
            for (Byte i : values) {
                if (getNumMoves(i) == 0) {
                    return true;
                }
            }
            return false;
        }

        private int getNumMoves(Byte value) {
            return value.byteValue() >> 2; 
        }

        private int getGameState(Byte value) {
            return value.byteValue() & 3;
        }

        private byte encodeState(int steps, int state) {
            return (byte) ((state & 3) | (steps << 2));
        }

        private boolean isWinState(Byte state, char mark) {
            if (mark == 'O') {
                return getGameState(state) == 1;
            } else {
                return getGameState(state) == 2;
            }
        }

        /**
         * The reduce function for the first mapreduce that you should be filling out.
         */
        @Override
        public void reduce(IntWritable key, Iterable<ByteWritable> values, Context context) throws IOException, InterruptedException {
            /* YOUR CODE HERE */
            //System.out.println("========SolveMoves REDUCE==========");
            //System.out.println(boardToString(boardToArray(key)));
            ArrayList<Byte> savedValues = new ArrayList<Byte>();
            for (ByteWritable t : values) {
                savedValues.add(new Byte(t.get()));
            }
            if (!isValidParent(savedValues)) {
                return;
            }
            
            char mark = 'O';
            char omark = 'X';
            if (!OTurn) {
                mark = 'X';
                omark = 'O';
            }

            // win(M) and lose(M) are functions to return win state or lose state for player M, respectively
            
            //set mark to the current layer player {'X','O'}
            int quickest_win = Integer.MAX_VALUE, slowest_lose = -1, slowest_tie = -1;
            Byte best_win = new Byte((byte)0);
            Byte best_tie = new Byte((byte)0);
            Byte best_lose = new Byte((byte)0);
            Byte best = new Byte((byte)0);

            
            
            //System.out.println("Values:");

            
            
            

            for (Byte v : savedValues) {
                //System.out.println(v.byteValue());
                if (isWinState(v, mark) && getNumMoves(v) < quickest_win) {
                    quickest_win = getNumMoves(v);
                    best_win = v;
                } else if (getGameState(v) == 3 && getNumMoves(v) > slowest_tie) {
                    slowest_tie = getNumMoves(v);
                    best_tie = v;
                } else if (isWinState(v, omark) && getNumMoves(v) > slowest_lose) {
                    slowest_lose = getNumMoves(v);
                    best_lose = v;
                } else {
                    //best = v;

                }
            }
            if (quickest_win < Integer.MAX_VALUE) {
                best = best_win;
            } else if (slowest_tie > -1) {
                best = best_tie;
            } else if (slowest_lose > -1) {
                best = best_lose;
            } else {
                //return;
                ////System.out.println("ERROR");
                ////System.out.println(boardToString(boardToArray(key)));
                ////System.out.println();
            }
          
            byte thisState = encodeState(getNumMoves(best) + 1, getGameState(best));

            int i, j;
            ArrayList<Integer> moves = new ArrayList<Integer>();
            /*
            for (i = 0; i < boardWidth; i++) {
                char[][] board = boardToArray(key);
                if (b2s(board) != Proj2Util.gameUnhasher(key.get(), boardWidth, boardHeight)) {
                    //System.out.println("HUGE MISTAKE");
                }
                for (j = boardHeight - 1; j >= 0 && board[i][j] == ' '; j--);
                if (j >= 0 && board[i][j] == omark) {
                    board[i][j] = ' ';
                    moves.add(boardToInt(board));
                    
                }

            }
            */
            
            for (i = 0; i < boardWidth; i++) {
                for (j = 0; j < boardHeight; j++) {
                    char[][] board = boardToArray(key);
                    if (board[i][j] == omark) {
                        board[i][j] = ' ';
                        ////System.out.println("=========");
                        //System.out.println("Gen Parent:   |" + boardToString(board));
                        moves.add(boardToInt(board));
                    }
                }

            }
            
            int finalMoves[] = new int[moves.size()];
            //System.out.println("Write:");
            for (i = 0; i < moves.size(); i++) {
                finalMoves[i] = moves.get(i).intValue();
                //System.out.println(Proj2Util.gameUnhasher(finalMoves[i], boardWidth, boardHeight));
            }
            MovesWritable out = new MovesWritable(thisState, finalMoves);
            //System.out.println("VALUE  :" + out.getValue());
            context.write(key, out);




            


        }
    }
}
