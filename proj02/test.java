public class test {
	static int boardWidth;
	static int boardHeight;
    private static int getNumMoves(byte value) {
        return (int)value >> 2; 
    }

    private static int getGameState(byte value) {
        return value & (byte)3;
    }

    private static byte encodeState(int steps, int state) {
        return (byte) ((state & 3) | (steps << 2));
    }

    private static boolean isWinState(byte state, char mark) {
        if (mark == 'O') {
            return getGameState(state) == 1;
        } else {
            return getGameState(state) == 2;
        }
    }

    public static void main(String[] args) {
    	System.out.println(encodeState(1, 2));
    }
}