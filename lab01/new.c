#include <stdio.h>

int main()
{
    int x[10], i;
    for (i = 0; i < 10; i++)
        x[i] = i;
    i = 5;
    int ans = x[i++] + x[i++] + x[i++];
    printf("%d\n", ans);
    return 0;
}
