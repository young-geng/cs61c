#include <stdio.h>

int main(void) {
	int n = 48;
	/* Don't worry too much about what char* means. 
	 * It's just C's way of declaring a string, and we'll
         * see why it looks so funny when we cover pointers. */
	char *str = "%d\n";

	printf("%c\n", n);
	printf(str, 0);

	return 0;
}
