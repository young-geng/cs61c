1. .data means initialized static data, and .word means the data type (32 bit), .text indicates the segment of instructions

2. set break points in exec window, program pauses before the line

3. press f7 to step, and uncheck breakpoints to run through the whole program

4. 34 is the 9th fib number

5. 0x10010010

6. go into data segment and change the 9 to 13

7. go to registers and change the number in the box

8. see help
